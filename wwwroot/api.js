executeRequest = function (request) {

    return $.ajax({
        url: request["URL"],
        data: JSON.stringify(request["DATA"]),
        dataType: "json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Authorization", "Basic " + btoa(request["USERNAME"] + ":" + request["PASSWORD"]));
        },
        type: "POST",
        contentType: "json"
    }).done(function (result) {
        return result;
    }).fail(function () {
        alert("fehler");
    });

}

getTableFromResponse = function (response) {

    var _tableResult = response["RESULT"]["TABLERESULT"];
    var _head = _tableResult["HEAD"];
    var _body = _tableResult["BODY"];

    var table = $("<table></table>").addClass("table");
    var _tHead = table.append("<thead></thead>");
    var _tBody = table.append("<tbody></tbody>");

    var _headerRow = $("<tr></tr>");
    _head.forEach(h => {
        _headerRow.append("<th>" + h["COLUMNNAME"] + "</th>");
    });
    _headerRow.append("<th>Actions</th>");
    _tHead.append(_headerRow);

    _body.forEach(b => {

        var row = $("<tr></tr>");
        _head.forEach(h => {
            row.append("<td>" + b[h["COLUMNNAME"]] + "</td>");
        });
        var _actionCell = $("<td></td>");

        _actionCell.append("<button title='Edit'>EDIT</button>");
        _actionCell.append("<button title='Remove'>REMOVE</button>");

        row.append(_actionCell);
        _tBody.append(row);

    });

    return table;

}

removeEntry = function (entry_id) {
    var request = createRequest("http://localhost", selected_schema.toUpperCase() + "_" + selected_table.toUpperCase() + "_" + "DELETEBYID", "root", "toor", entry_id);
    executeRequest(request).then(function (response){
        alert(JSON.stringify(response));
    });
}

getTableFromRequest = function (request) {
    return executeRequest(request).then(function (response) {
        return getTableFromResponse(response);
    });
}

createRequest = function (url, command, username, password, id) {

    return {
        USERNAME: username,
        PASSWORD: password,
        URL: url,
        DATA: {
            ID: id,
            COMMAND: command,
            PARAMETERS:
                {

                }
        }
    };

}

addParameterToRequest = function (request, pname, pvalue) {

    request["PARAMETERS"][pname] = pvalue;
    return request;

}

getGenericTables = function (url, username, password) {

    var request = createRequest(url, "GET_GENERIC_TABLES", username, password, -1);
    executeRequest(request);

    var response = executeRequest(request).then(function (res) {
        return res["RESULT"];
    });

    return response;
}