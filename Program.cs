﻿using System;
using de.kuerschner.core.server;

namespace de.kuerschner.core.testserver
{
    // more at: www.tobiaskuerschner.de
    class Program
    {
        static void Main(string[] args)
        {
            var _server = new HTTPServer();
            _server.Start(args);
        }
    }
}
